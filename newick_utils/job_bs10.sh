if test -f "/userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_boot1st/L${1}_boot1st.treefile"; then
    nw_ed  /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_boot1st/L${1}_boot1st.treefile 'i & b<=10' o > /userdata/dowdy/ahe_phase2/all_by_gene/condensed_gene_trees_extracted/L${1}_boot1st-BS10.treefile
fi
if test -f "/userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted/L${1}_shrunk.treefile"; then
    nw_ed  /userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted/L${1}_shrunk.treefile 'i & b<=10' o > /userdata/dowdy/ahe_phase2/all_by_gene/condensed_shrunk_trees_extracted/L${1}_shrunk-BS10.treefile
fi