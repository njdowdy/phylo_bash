if test -f "/userdata/dowdy/ahe_phase2/all_data/loci/T657_Dowdy_Erebidae5_L${1}.phylip"; then
    mkdir -p /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees/L${1}/RUN${2}
    /iqtree-2.1.2-Linux/bin/iqtree2 -nt 2 -seed ${2} -s /userdata/dowdy/ahe_phase2/all_data/loci/T657_Dowdy_Erebidae5_L${1}.phylip --prefix /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees/L${1}/RUN${2}/L${1} -m MFP -msub nuclear -cmax 15 -madd LG+C60,LG4X,LG4M -B 2000 -alrt 2000 -abayes -lbp 2000 -bnni -wbtl
fi