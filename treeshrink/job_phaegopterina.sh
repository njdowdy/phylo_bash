find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/ -type f -name '*.treefile' -exec sed -i 's/YAN14_0004\/_AHE19_0111/YAN14_0004_AHE19_0111/g' {} \;
find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/ -type f -name '*.fasta' -exec sed -i 's/YAN14_0004\/"AHE19_0111"/YAN14_0004_AHE19_0111_/g' {} \;
find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/ -type f -name '*.fasta' -exec sed -i 's/"0181"/_0181_/g' {} \;
mkdir /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/
echo "mkdir /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/L\${1}_boot1st" > /dir.sh
chmod +x /dir.sh
parallel /dir.sh ::: {1..920}
cp /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_tree_files_boot1st/* /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/
cp /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_data/* /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/
rename 's/^(.+)\.(.+)/$1\/input.$2/s' /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/*.treefile
rename 's/^(.+)\.(.+)/$1_boot1st\/alignment.$2/s' /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/*.fasta
find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/ -empty -type d -delete
/TreeShrink/run_treeshrink.py --mode per-species --centroid --quantiles "0.05" --outdir /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/treeshrink --outprefix shrunk --indir /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/gene_trees_extracted/ --tree input.treefile --alignment alignment.fasta --exceptions I4266_JZJM112_AHEJZ079_Erebidae_NA_Acyphas_chionitis_seq1,I4267_PUS_AHEJZ067_Erebidae_NA_Lymantria_dispar_seq1,I4265_JZTH028_AHEJZ054_Erebidae_NA_Asota_ficus_seq1
mkdir /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/shrunk_trees_extracted
cp -r /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/treeshrink/* /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/shrunk_trees_extracted/
find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/shrunk_trees_extracted -name "*.txt" -delete
rename 's/(.*)\/(L[0-9]+)\//$1\/$2_/' /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/shrunk_trees_extracted/*/*
find /userdata/dowdy/ahe_phase2/phaegopterina_by_gene/shrunk_trees_extracted/ -empty -type d -delete