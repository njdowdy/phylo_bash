find ./ -type f -exec sed -i 's/YAN14_0004\/_AHE19_0111/YAN14_0004_AHE19_0111/g' {} \;
echo "mkdir /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_extracted/L\${1}" > /dir.sh
chmod +x /dir.sh
parallel /dir.sh ::: {1..838}
rename 's/^(.+)\.(.+)/$1\/input.$2/s' /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_extracted/*.treefile
find /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_extracted/ -empty -type d -delete
/TreeShrink/run_treeshrink.py --mode per-species --centroid --quantiles "0.05" --outdir /userdata/dowdy/ahe_phase2/all_by_gene/treeshrink --outprefix shrunk --indir /userdata/dowdy/ahe_phase2/all_by_gene/gene_trees_extracted/ --tree input.treefile
cp -r /userdata/dowdy/ahe_phase2/all_by_gene/treeshrink/* /userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted/
find /userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted -name "*.txt" -delete
rename 's/(.*)\/(L[0-9]+)\//$1\/$2_/' /userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted/*/*
find /userdata/dowdy/ahe_phase2/all_by_gene/shrunk_trees_extracted/ -empty -type d -delete